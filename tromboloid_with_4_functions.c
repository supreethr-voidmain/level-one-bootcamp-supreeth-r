//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float hgt( float h)
{
	printf("Enter the height of the tromboloid: \n");
	scanf("%f", &h);
    while(h==0)
    {
        printf("Enter a positive height: \n");
        scanf("%f",&h);
    }
	return h;
}
float bth( float b)
{
	printf("Enter the breadth of the tromboloid: \n");
	scanf("%f",&b);
	while(b<=0)
	{
	    printf("Enter a positive breadth: \n");
        scanf("%f",&b);
	}
	return b;
}
float dpt(float d)
{
	printf("Enter the depth of the tromboloid: \n");
	scanf("%f", &d);
	while(d<=0)
	{
	    printf("Enter a positive depth: \n");
        scanf("%f",&d);
	}
	return d;
}
float vol( float h, float b, float d)
{
	return ((h*b*d) + (d/b))/3;
}
int main()
{
    float h,b,d;
    h= hgt(h);
    b= bth(b);
    d= dpt(d);
    printf("The volume of tromboloid whose height is %f, breadth is %f, and depth is %f is %f.\n", h, b, d, vol(h,b,d));
}
