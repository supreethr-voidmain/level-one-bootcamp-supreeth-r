//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
float vol(float b, float d , float h)
{
	return ((h*b*d)+(d/b))/3;
}
int main()
{
	float h,b,d;
	printf(" Enter height of the tromboloid: \n");
	scanf("%f", &h);
	printf("Enter breadth of the tromboloid: \n");
	scanf("%f", &b);
	printf("Enter depth of the tromboloid: \n");
	scanf("%f", &d);
	printf("volume of tromboloid for %f height, %f breadth, %f depth is %f.\n", h,b,d,vol(h,b,d));
}

